<?php

class Posts_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function create_post($user_id,$title,$text,$type){

        $data = array(
            'user_id' => $user_id,
            'title' => $title,
            'text' => $text,
            'type' => $type
        );
        $this->db->insert('posts', $data);
    }

    public function get_userposts($user_id)
    {

        $this->db->where('user_id',$user_id);

        $query = $this->db->get('posts');

        return $query->result_array();

    }

    public function get_requestposts()
    {

        $this->db->where('type','requests');

        $query = $this->db->get('posts');

        return $query->result_array();

    }

    public function get_developersposts()
    {

        $this->db->where('type','developers');

        $query = $this->db->get('posts');

        return $query->result_array();

    }

    public function delete_post($post_id){
        $this->db->delete('posts', array('id' => $post_id));
    }

    public function get_allposts(){


    }

    public function get_post($post_id){

        $this->db->where('post_id',$post_id);

        $query = $this->db->get('posts');

        return $query->result_array();
    }



    public function add_post($user_id,$title,$text){

        $data = array(
            'user_id' => $user_id,
            'title' => $title,
            'text' => $text,
        );
        $this->db->insert('posts',$data);
    }

}