<?php

class Posts extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('image_lib');
        $this->load->model('posts_model');
        $this->load->model('users/account_model');
    }

    public function add_post(){
        $this->load->view('users/add_post');
    }

    function create_post()
    {
        $user_id = $this->session->userdata('userID');

        $user_type = $this->account_model->get_usertype($user_id);

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('title', 'Title', 'trim|xss_clean|required|min_length[3]');
        $this->form_validation->set_rules('text', 'Text', 'trim|xss_clean|required|min_length[3]');

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('add_post');
        } else {

            $title = $this->input->post('title');

            $text = $this->input->post('text');
            $type = $user_type['type'];

            $this->posts_model->create_post($user_id, $title, $text,$type);


            redirect(base_url() . 'index.php/users/account/index');

        }
    }

    function view(){

        $post_id = $this->input->get('post_id');

        $data['post'] = $this->posts_model->get_post($post_id)[0];

        $this->load->view('index/templates/header');
        $this->load->view('view',$data);
        $this->load->view('index/templates/footer');

    }

    function delete_post(){

        $post_id = $this->input->get('id');

        $this->posts_model->delete_post($post_id);

        redirect(base_url() . 'index.php/users/account/index');

    }
}
