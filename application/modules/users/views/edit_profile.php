
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/style.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>../favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/component.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>/js/modernizr.custom.js"></script>
</head>
<body>
<form method="post" action="<?php echo base_url(); ?>index.php/users/account/edit">
<input type="hidden" name="id" value="<?php echo $info['id']; ?>">
    <p><label>Ваше имя:</label><br/>

        <input type="text" name="username" value="<?php echo $info['username']; ?>"/></p>
    <?php echo form_error('username'); ?>
    <br/>

    <p><label>E-mail:</label><br/>

    <div>
        <input type="text" name="email" value="<?php echo $info['email']; ?>"/>
        <?php echo form_error('email'); ?>
    </div>
    </p>

    <br/>

    <p><label>Тип пользователя</label><br/>
    <div>
        <select name="type" value="">
            <option value="developers">Developers</option>
            <option value="customers">Customers</option>

        </select>
        <?php echo form_error('type'); ?>
    </div>
    </p>
    <br/>

    <p><label>Местонахождение</label><br/>
    <div>
        <input type="text" name="location" value="<?php echo $info['location'] ?>"/>
        <?php echo form_error('location'); ?>
    </div>
    </p>
    <br/>

    <p><label>Телефон</label><br/>
    <div>
        <input type="text" name="phone" value="<?php echo $info['phone'] ?>"/>
        <?php echo form_error('phone'); ?>
    </div>
    </p>
    <br/>

    </p>
    <br/>

    <p><label>О себе</label><br/>
    <div>
        <input type="textarea" name="description" value="<?php echo $info['description']; ?>"/>
        <?php echo form_error('description'); ?>
    </div>
    </p>
    <br/>

    </p>
    <br/>



    <input type="submit" value="Update"/>

</form>


<style>
    input img {float:left;}
    div.error {color: red}
</style>
