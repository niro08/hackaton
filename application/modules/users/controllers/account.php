<?php

class Account extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('account_model');
        $this->load->model('posts/posts_model');
    }

    public function index()
    {

        if ($this->input->get()) {

            $data['info'] = $this->input->get();

            $this->load->view('index/templates/header');
            $this->load->view('cabinet', $data);
            $this->load->view('index/templates/footer');

        } else {
            $user_id = $this->session->userdata('userID');

            $data['info'] = $this->account_model->get_userinfo($user_id);
            $data['posts'] = $this->posts_model->get_userposts($user_id);

            $this->load->view('index/templates/header');
            $this->load->view('cabinet', $data);
            $this->load->view('index/templates/footer');

        }

    }

    public function edit_profile()
    {

        $user_id = $this->session->userdata('userID');

        $data['info'] = $this->account_model->get_userinfo($user_id);

        $this->load->view('edit_profile', $data);
    }

    public function edit()
    {
        $user_id = $this->session->userdata('userID');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('username', 'Name', 'trim|xss_clean|required|min_length[3]|max_length[25]');
        $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required|valid_email|callback_emailExists');
        $this->form_validation->set_rules('type', 'Customer type', 'trim|xss_clean|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|xss_clean|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|xss_clean|required|numeric');

        if ($this->form_validation->run() == FALSE) {


            $info = array();

            $info['id'] = $user_id;
            $info['username'] = $this->input->post('username');
            $info['email'] = $this->input->post('email');
            $info['type'] = $this->input->post('type');
            $info['photo'] = $this->input->post('photo');
            $info['location'] = $this->input->post('location');
            $info['phone'] = $this->input->post('phone');
            $info['description'] = $this->input->post('description');
            $data['info'] = $info;

            $this->load->view('edit_profile', $data);
        } else {

            $username = $this->input->post('username');

            $email = $this->input->post('email');

            $type = $this->input->post('type');

            $photo = $this->input->post('photo');

            $location = $this->input->post('location');

            $phone = $this->input->post('phone');

            $desc = $this->input->post('description');


            $this->account_model->edit_profile($user_id, $username, $email, $type, $photo, $location, $phone, $desc);


            redirect(base_url() . 'index.php/users/account/index');

        }
    }

}