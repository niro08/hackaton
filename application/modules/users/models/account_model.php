<?php

class Account_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_userinfo($user_id)
    {

        $this->db->where('id',$user_id);

        $query = $this->db->get('users');

        $result = $query->result_array()[0];
        return $result;

    }

    public function get_usertype($user_id){

        $this->db->select('type');
        $this->db->where('id',$user_id);
        $query = $this->db->get('users');
        $result = $query->result_array()[0];
        return $result;
    }

    public function get_username($user_id){

        $this->db->select('username');
        $this->db->where('id',$user_id);
        $query = $this->db->get('users');
        $result = $query->result_array()[0];
        return $result;
    }

    public function edit_profile($user_id,$username, $email, $type, $photo, $location, $phone, $desc){

        $data = array(
            'username' => $username,
            'email' => $email,
            'type' => $type,
            'photo' => $photo,
            'location' => $location,
            'phone' => $phone,
            'description' => $desc,

        );
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);

    }



}