


 <script>
    (function($) {
        $(function() {

            $('ul.tabs').on('click', 'li:not(.current)', function() {
                $(this).addClass('current').siblings().removeClass('current')
                    .parents('div.section').find('div.box').eq($(this).index()).fadeIn(150).siblings('div.box').hide();
            })

        })
    })(jQuery)

</script>
<div class="contentPage">
 <div class="section">

    <ul class="tabs">
        <li class="current"><h2> Requests Posts:</h2></li>
        <li class=""><h2> Developers Posts:</h2></li>

    </ul>

    <div class="box visible" style="display: block;">
        <p><?php foreach($requests as $request): ?>

        <h1><a href="index.php/users/account?username=<?php echo $request['username']?>&type=<?php echo $request['type']?>&email=<?php echo $request['email']?>&description=<?php echo $request['description']?>&photo=<?php echo $request['photo']?>&location=<?php echo $request['location']?>&phone=<?php echo $request['phone']?>"><?php echo $request['username'] ?></a></h1>
        <h2><a href="index.php/posts/posts/view?post_id=<?php echo $request['post_id'] ?>"><?php echo $request['title'] ?></a></h2>
        <p><?php echo $request['text'] ?></p>
        <?php endforeach; ?></p>
    </div>

    <div class="box" style="display: none;">
        <p><?php foreach($developers as $develop): ?>

        <h1><a href="index.php/users/account?username=<?php echo $develop['username']?>&type=<?php echo $develop['type']?>&email=<?php echo $develop['email']?>&description=<?php echo $develop['description']?>&photo=<?php echo $develop['photo']?>&location=<?php echo $develop['location']?>&phone=<?php echo $develop['phone']?>"><?php echo $develop['username'] ?></a></h1>
        <h2><a href="index.php/posts/posts/view?post_id=<?php echo $develop['post_id'] ?>"><?php echo $develop['title'] ?></a></h2>
        <p><?php echo $develop['text'] ?></p>
        <?php endforeach; ?></p>
    </div>

 </div>
</div>
