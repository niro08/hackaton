<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/style.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>../favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/component.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>/js/modernizr.custom.js"></script>
</head>
<body>
<div class="logForms">
    <script type="text/javascript">
        function getRegForm() {
            window.location.replace("/index.php/login/users/registration_form");
            return false;
        }
        function getLoginForm() {
            window.location.replace("/index.php/login/users/login_form");
            return false;
        }
    </script>
    <?php if($this->session->userdata('userID') != False): ?>
        <button class="btn btn-8 btn-8a"><h5 class="logout"><p><a href="/index.php/login/users/logout" class="asd">Выйти</a></p></button>
        <button class="btn btn-8 btn-8a"><h5 class="logout"><p><a href="/index.php/users/account/index" class="asd"><?php echo $this->session->userdata('username') ?></a></p></button>
        <button class="btn btn-8 btn-8a"><h5 class="logout"><p><a href="/index.php/" class="asd">На главную</a></p></button>



    <?php else: ?>

        <button class="btn btn-8 btn-8c" onclick="getLoginForm()"><a class="loginT">Вход</a></button>
        <div class="clear"></div>
        <button class="btn btn-8 btn-8d" onclick="getRegForm()"><a class="regB">Регистрация</a></button>
    <?php endif; ?>
</div>
<div class="clear"></div>
<div class="headImfBackground">
<div class="headImage"></div>
    <div class="headText"><h4 class="headTeam">Karma Team</h4></div>
</div>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-550ead26339d0334" async="async"></script>
