<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('posts/posts_model');
        $this->load->model('users/account_model');
    }

    public function index()
    {

        $posts = $this->posts_model->get_requestposts();
        $requests = array();
        foreach($posts as $post){
          $user_id = $post['user_id'];
            $userinfo =   $this->account_model->get_userinfo($user_id);
          $requests[] = array_merge($post,$userinfo);

        }
        $data['requests'] = $requests;

        $posts1 = $this->posts_model->get_developersposts();
        $develops = array();
        foreach($posts1 as $develop){
            $user_id = $develop['user_id'];
            $userinfo =   $this->account_model->get_userinfo($user_id);
            $develops[] = array_merge($develop,$userinfo);

        }
        $data['developers'] = $develops;
            $this->load->view('/templates/header');
            $this->load->view('index',$data);
            $this->load->view('/templates/footer');


    }


}
