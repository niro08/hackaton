<?php
class Comment_model extends CI_Model {

    public function __construct()
    {

        $this->load->database();
    }

    public function get_comments()
    {

             $query = $this->db->get('comment');

            return $query->result_array();

    }

    public function set_comments()
    {
        $this->load->helper('url');



        $data = array(
            'name' => $this->session->userdata('userName'),
            'text' => $this->input->post('text'),
            'date' => date("Y-m-d H:i:s")
        );

        return $this->db->insert('comment', $data);

    }
}