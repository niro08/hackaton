<?php

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('image_lib');
        $this->load->model('users_model');
    }

    function index()
    {
        $this->load->view('login_form');
    }

    function registration_form()
    {
        $this->load->view('registration_form');

    }

    function login_form(){

        $this->load->view('login_form');

}

    function register()
    {


        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('username', 'Name', 'trim|xss_clean|required|min_length[3]|max_length[25]');
        $this->form_validation->set_rules('email', 'Email', 'trim|xss_clean|required|valid_email|callback_emailExists');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|required|min_length[6]');
        $this->form_validation->set_rules('conf_password', 'Confirm password', 'required|min_length[6]|matches[password]');
        $this->form_validation->set_rules('type', 'Customer type', 'trim|xss_clean|required');
        $this->form_validation->set_rules('location', 'Location', 'trim|xss_clean|required');
        $this->form_validation->set_rules('phone', 'Phone', 'trim|xss_clean|required|numeric');



        if ($this->form_validation->run() == FALSE) {

            $this->load->view('registration_form');
        } else {

            $username = $this->input->post('username');

            $email = $this->input->post('email');

            $password = $this->input->post('password');

            $type = $this->input->post('type');

            $photo = $this->input->post('photo');

            $location = $this->input->post('location');

            $phone = $this->input->post('phone');

            $desc = $this->input->post('desc');


            $this->users_model->create_user($username, $password, $email, $type, $photo, $location, $phone,$desc);

            $userid = $this->db->insert_id();

            $this->session->set_userdata('userID', $userid);

//            $this->load->view('index/templates/header');
//            $this->load->view('index/index');
//            $this->load->view('index/templates/footer');
            redirect(base_url() . 'index.php/users/account/index');

        }


    }

    function login()
    {


        $email = $this->input->post('email');

        $password = $this->input->post('password');

        $passwordx = sha1($password);

        $query = $this->db->query("SELECT * FROM users WHERE email='$email' AND password='$passwordx'");

        if ($query->num_rows() == 1) {

            $userID = $query->row()->id;

            $username = $query->row()->username;

            $this->session->set_userdata('userID', $userID);

            $this->session->set_userdata('username', $username);



            redirect(base_url());
//            $this->load->view('index/templates/header');
//            $this->load->view('index/index');
//            $this->load->view('index/templates/footer');


        } else {

            $data['error'] = 'The username or password not correct. Please, try again';

            $this->load->view('login_form', $data);


        }

    }

    function logout()
    {


        $this->session->unset_userdata('userID');


        redirect(base_url());
//        $this->load->view('index/templates/header');
//        $this->load->view('index/index');
    }

    public function emailExists($str)
    {

        return $this->users_model->emailExists($str);
    }

}