<?php
class User extends CI_Model
{
    public function __construct()
    {

        parent::__construct();

    }

    function create_user ($username, $password, $email, $type, $photo, $location, $phone, $desc)
    {
        $data = array(
            'username' => $username,
            'password' => sha1($password),
            'email' => $email,
            'type' => $type,
            'photo' => $photo,
            'location' => $location,
            'phone' => $phone,
            'description' => $desc,

        );
        $this->db->insert('users', $data);

    }

    function emailExists($str){

        $this->db->where('email',$str);
        $query = $this->db->get('users');

        if ($query->num_rows != 0 ){
            $this->form_validation->set_message('emailExists','The user with such email is already registered');
            return FALSE;
        } else {

            return TRUE;
        }

    }

}