
<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/style.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>../favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/component.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>/js/modernizr.custom.js"></script>
</head>
<body>
<div class="fffform">
<form class="regForm1" method="post" action="<?php echo base_url(); ?>index.php/login/users/register">

    <p class="name"><label style="color: #FFF">Ваше имя:</label><br/>

        <input class="username12" type="text" name="username" value="<?php echo set_value('username'); ?>"/></p>
     <?php echo form_error('username'); ?>
    <br/>
    <p><label style="color: #FFF">Пароль:</label><br/>

        <input type="password" name="password" value="<?php echo set_value('password'); ?>"/></p>
    <?php echo form_error('password'); ?>
    <br/>

    <p><label style="color: #FFF">Подтвердите пароль:</label><br/>

        <input type="password" name="conf_password" value="<?php echo set_value('conf_password'); ?>"/></p>
    <?php echo form_error('conf_password'); ?>
    <br>
    <p class="emalR"><label style="color: #FFF">E-mail:</label><br/>

    <div class="emalr2">
        <input  class="emaldfl" type="text" name="email" value="<?php echo set_value('email'); ?>"/>
        <?php echo form_error('email'); ?>
    </div>
    </p>

    <br/>

    <p class="userType"><label style="color: #FFF">Тип пользователя</label><br/>
    <div class="userTypeSelect">
        <select name="type" value="">
            <option value="developers" >Developers</option>
            <option value="requests">Customers</option>

        </select>
        <?php echo form_error('type'); ?>
    </div>
    </p>
    <br/>

    <p class="location1"><label style="color: #FFF">Местонахождение</label><br/>
    <div class="locintup">
        <input type="text" name="location" value="<?php echo set_value('location'); ?>"/>
        <?php echo form_error('location'); ?>
    </div>
    </p>
    <br/>

    <p class="phone1"><label style="color: #FFF">Телефон</label><br/>
    <div class="phoneInput">
        <input type="text" name="phone" value="<?php echo set_value('phone'); ?>"/>
        <?php echo form_error('phone'); ?>
    </div>
    </p>
    <br/>

    </p>
    <br/>

    <p class="descr1"><label style="color: #FFF">О себе</label><br/>
    <div class="descrField">
        <input type="text" name="description" value="<?php echo set_value('description'); ?>"/>
        <?php echo form_error('description'); ?>
    </div>
    </p>
    <br/>

    </p>
    <br/>

<input class="regsubmit" type="submit" value="Register"/>

</form>
</div>

<style>
    input img {float:left;}
    div.error {color: red}
</style>
