<!DOCTYPE html>
<html>
<head>
    <meta charset='utf-8'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/css/style.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>../favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/component.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>/js/modernizr.custom.js"></script>
</head>
<body>
<div class="loginForm">
    <div style="color: #fff; position: relative; top:  10px;">
        <?php if(isset ($error)) echo $error ;?>
    </div>

    <p><strong style="color: #5cbcf6">Вход</strong></p>

    <form method="post" action="<?php echo base_url(); ?>index.php/login/users/login">

        <p class="email1"><label style="color: #fff">Email:</label>

            <input type="text" name="email" value="" /></p>

        <p class="passwd"><label style="color: #fff">Пароль:</label>

            <input type="password" name="password" value="" /></p>

         <input id="loginb" type="submit" value=" Вход" />

<?php echo form_close();?>

</div>